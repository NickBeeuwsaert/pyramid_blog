pyramid_blog
============

Simple blog written in pyramid

Set up
------
Run these commands:

```
$ pyvenv venv
$ venv/bin/python develop
$ venv/bin/alembic upgrade head
```

Running development server:

```
$ venv/bin/pserve development.ini --reload
```