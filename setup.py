from setuptools import setup

requires = [
    'pyramid',
    'pyramid_tm',
    'pyramid_jinja2',
    'pyramid_beaker',
    'alembic',
    'SQLAlchemy',
    'zope.sqlalchemy',
    'WTForms',
    'passlib'
]

setup(
    name="pyramid_blog",
    version="0.0.1",
    license="MIT",
    install_requires=requires,
    test_suite="tests",
    entry_points={
        "paste.app_factory": [
            "main = pyramid_blog:main"
        ]
    }
)
