from wtforms import Form as BaseForm
from wtforms import fields
from wtforms import validators
from wtforms import ValidationError
from .models import User


class field_taken(object):
    def __init__(self, column, message='Not available'):
        self.column = column
        self.message = message

    def __call__(self, form, field):
        db = form.request.db
        if db.query(self.column).filter(self.column == field.data).first():
            raise ValidationError(self.message)


class Form(BaseForm):
    def __init__(self, request, *args, **kwargs):
        self.request = request
        super().__init__(*args, **kwargs)


class RegistrationForm(Form):
    username = fields.StringField('Username', [
        validators.required(),
        field_taken(User.username, message='That username is already taken')
    ])
    email = fields.StringField('Email', [
        validators.required(),
        validators.email(),
        field_taken(User.email, message='That email is already in use')
    ])
    password = fields.PasswordField('Password', [
        validators.required(),
        validators.equal_to('confirm_password', message='Passwords must match')
    ])
    confirm_password = fields.PasswordField('Confirm Password')


class LoginForm(Form):
    username = fields.StringField('Username')
    password = fields.PasswordField('Password')


class PostForm(Form):
    title = fields.StringField('Title', [
        validators.required()
    ])
    content = fields.TextAreaField('Content', [
        validators.required()
    ])
