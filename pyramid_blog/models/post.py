from .meta import Base
import sqlalchemy as sa
from datetime import datetime
from pyramid.security import Allow


class Post(Base):
    __tablename__ = 'posts'

    id = sa.Column(sa.Integer, primary_key=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'))
    title = sa.Column(sa.String)
    content = sa.Column(sa.String)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    modified_at = sa.Column(
        sa.DateTime,
        default=datetime.now,
        onupdate=datetime.now
    )

    @property
    def __acl__(self):
        return [
            (Allow, self.owner.username, 'edit'),
            (Allow, self.owner.username, 'delete')
        ]
