from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
import zope.sqlalchemy

from .user import User
from .post import Post


def get_session(session_factory, transaction_manager):
    session = session_factory()

    zope.sqlalchemy.register(session, transaction_manager=transaction_manager)
    return session


def includeme(config):
    settings = config.get_settings()
    engine = engine_from_config(settings)
    Session = sessionmaker(bind=engine)

    config.add_request_method(
        lambda r: get_session(Session, r.tm),
        'db',
        reify=True
    )
