from .meta import Base
import sqlalchemy as sa
from sqlalchemy.orm import relationship
from datetime import datetime
from passlib.apps import custom_app_context


class User(Base):
    __tablename__ = 'users'

    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String(256), unique=True)
    password = sa.Column(sa.String(100))
    email = sa.Column(sa.String(256), unique=True)
    created_at = sa.Column(sa.DateTime, default=datetime.now)
    modified_at = sa.Column(
        sa.DateTime,
        default=datetime.now,
        onupdate=datetime.now
    )

    posts = relationship('Post', backref='owner')

    def set_password(self, password):
        self.password = custom_app_context.encrypt(password)

    def verify_password(self, password):
        return custom_app_context.verify(password, self.password)
