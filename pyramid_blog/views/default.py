from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound


@view_config(route_name='index')
def index(request):
    return HTTPFound(
        location=request.route_path('posts.index')
    )


def includeme(config):
    config.add_route('index', '/')

    config.scan(__name__)
