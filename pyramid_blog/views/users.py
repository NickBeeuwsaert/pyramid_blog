from pyramid.view import view_config
from pyramid.security import remember, forget
from pyramid.httpexceptions import HTTPFound
from ..forms import RegistrationForm, LoginForm
from ..models import User
from .. import resources


class UsersView(object):
    def __init__(self, request):
        self.request = request

    @view_config(route_name='users.register', renderer='users/register.jinja2')
    def register(self):
        form = RegistrationForm(self.request, self.request.POST)
        if self.request.method == 'POST' and form.validate():
            user = User(
                username=form.username.data,
                email=form.email.data
            )
            user.set_password(form.password.data)
            self.request.db.add(user)
            return HTTPFound(
                location=self.request.route_path('posts.index')
            )
        return dict(form=form)

    @view_config(route_name='users.login', renderer='users/login.jinja2')
    def login(self):
        form = LoginForm(self.request, self.request.POST)
        if self.request.method == 'POST' and form.validate():
            db = self.request.db
            user = db.query(User)\
                .filter(User.username == form.username.data)\
                .first()
            if user and user.verify_password(form.password.data):
                return HTTPFound(
                    headers=remember(self.request, user.username),
                    location=self.request.route_path('posts.index')
                )
            else:
                self.request.session.flash(
                    'Invalid username or password',
                    'error'
                )
        return dict(form=form)

    @view_config(route_name='users.logout')
    def logout(self):
        self.request.session.flash('You have been logged out', 'success')
        return HTTPFound(
            headers=forget(self.request),
            location=self.request.route_path('posts.index')
        )

    @view_config(route_name='users.view', renderer='users/view.jinja2')
    def view(self):
        user = self.request.context
        return dict(user=user, posts=user.posts)


def includeme(config):
    config.add_route('users.register', '/register')
    config.add_route('users.login', '/login')
    config.add_route('users.logout', '/logout')
    config.add_route('users.view', '/view/{username}',
                     factory=resources.UserFactory, traverse='/{username}')
    config.scan(__name__)
