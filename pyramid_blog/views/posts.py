from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound
import sqlalchemy as sa
from ..forms import PostForm
from ..models import Post
from .. import resources


class PostsView(object):

    def __init__(self, request):
        self.request = request

    @view_config(
        route_name='posts.create',
        renderer='posts/create.jinja2',
        permission='create'
    )
    def create(self):
        '''Create a new post, associated with the current user'''
        form = PostForm(self.request, self.request.POST)

        if self.request.method == 'POST' and form.validate():
            post = Post(
                owner=self.request.user,
                title=form.title.data,
                content=form.content.data
            )
            self.request.db.add(post)
            self.request.db.flush()

            return HTTPFound(
                location=self.request.route_path('posts.view', post_id=post.id)
            )
        return dict(form=form)

    @view_config(route_name='posts.view', renderer='posts/view.jinja2')
    def read(self):
        '''Display post with the passed ID'''
        return dict(post=self.request.context)

    @view_config(
        route_name='posts.edit',
        renderer='posts/edit.jinja2',
        permission='edit'
    )
    def update(self):
        '''Display a form to update the specified post'''
        post = self.request.context
        form = PostForm(self.request, self.request.POST, post)
        if self.request.method == 'POST' and form.validate():
            form.populate_obj(post)

            return HTTPFound(
                location=self.request.route_path('posts.view', post_id=post.id)
            )
        return dict(form=form)

    @view_config(route_name='posts.delete', renderer='posts/delete.jinja2')
    def delete(self):
        return {}

    @view_config(route_name='posts.index', renderer='posts/index.jinja2')
    def index(self):
        '''Show the 10 most recent posts'''
        posts = self.request.db.query(Post).order_by(
            sa.desc(Post.created_at)
        ).limit(10).all()
        return dict(posts=posts)


def includeme(config):
    config.add_route('posts.index', '/')
    config.add_route('posts.create', '/create',
                     factory=resources.PostFactory)
    config.add_route('posts.view', '/view/{post_id}',
                     factory=resources.PostFactory, traverse='/{post_id}')
    config.add_route('posts.edit', '/edit/{post_id}',
                     factory=resources.PostFactory, traverse='/{post_id}')
    config.add_route('posts.delete', '/delete/{post_id}')

    config.scan(__name__)
