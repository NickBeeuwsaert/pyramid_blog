from .models import User, Post
from pyramid.security import Allow
from pyramid.security import Everyone, Authenticated


class UserFactory(object):
    def __init__(self, request):
        self.request = request

    def __getitem__(self, item):
        db = self.request.db
        user = db.query(User).filter(User.username == item).first()
        if not user:
            raise KeyError
        return user


class PostFactory(object):
    __acl__ = [
        (Allow, Everyone, 'view'),
        (Allow, Authenticated, 'create')
    ]

    def __init__(self, request):
        self.request = request

    def __getitem__(self, item):
        post = self.request.db.query(Post).filter(Post.id == item).first()
        if not post:
            raise KeyError
        return post
