from pyramid.config import Configurator
from pyramid.authentication import SessionAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy
from .security import get_user


def main(global_config, **settings):
    authn_policy = SessionAuthenticationPolicy()
    authz_policy = ACLAuthorizationPolicy()

    config = Configurator(
        settings=settings,
        authentication_policy=authn_policy,
        authorization_policy=authz_policy
    )

    config.add_request_method(get_user, 'user', reify=True)
    config.add_static_view('images', 'pyramid_blog:static/')
    config.include('.models')
    config.include('.views.posts', route_prefix='/posts')
    config.include('.views.users', route_prefix='/users')
    config.include('.views.default', route_prefix='/')

    return config.make_wsgi_app()
