from pyramid.security import unauthenticated_userid
from .models import User


def get_user(request):
    userid = unauthenticated_userid(request)

    return request.db.query(User).filter(User.username == userid).first()
