"""Create users table

Revision ID: 9c79a87166ab
Revises: 
Create Date: 2016-04-23 13:12:21.141053

"""

# revision identifiers, used by Alembic.
revision = '9c79a87166ab'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('username', sa.String(256), unique=True),
        sa.Column('password', sa.String(100)),
        sa.Column('email', sa.String(256), unique=True),
        sa.Column('created_at', sa.DateTime),
        sa.Column('modified_at', sa.DateTime)
    )


def downgrade():
    op.drop_table('users')
