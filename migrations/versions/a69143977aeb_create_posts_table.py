"""Create posts table

Revision ID: a69143977aeb
Revises: 9c79a87166ab
Create Date: 2016-04-23 13:21:28.095593

"""

# revision identifiers, used by Alembic.
revision = 'a69143977aeb'
down_revision = '9c79a87166ab'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('posts',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, sa.ForeignKey('users.id')),
        sa.Column('title', sa.String),
        sa.Column('content', sa.String),
        sa.Column('created_at', sa.DateTime),
        sa.Column('modified_at', sa.DateTime)
    )


def downgrade():
    op.drop_table('posts')
